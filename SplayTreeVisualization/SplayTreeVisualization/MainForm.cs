﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SplayTreeVisualization
{
    public partial class MainForm : Form
    {
        private OperationList operationList;
        private SplayTree splayTree;
        private Graphics graphics;

        public MainForm()
        {
            InitializeComponent();

            splayTree = new SplayTree();
            operationList = new OperationList();
            operationListBindingSource = new BindingSource();
            operationListBindingSource.DataSource = operationList;
            dataGridView.DataSource = operationListBindingSource;
            graphics = pictureBox.CreateGraphics();
            graphics.TranslateTransform(350, 150);
            currentScale = 1.0f;
        }

        private void updateDataGridView()
        {
            operationListBindingSource = new BindingSource();
            operationListBindingSource.DataSource = operationList;
            dataGridView.DataSource = operationListBindingSource;
            dataGridView.Update();
            dataGridView.Refresh();
            dataGridView.ClearSelection();
            if (dataGridView.RowCount > 0)
            {
                dataGridView.FirstDisplayedScrollingRowIndex = dataGridView.RowCount - 1;
            }
            performedOperationsLabel.Text = "Performed " + (operationList == null ? 0 : operationList.Count) + " operations.";
        }

        private void drawStep(TreeVisualData tvd)
        {
            graphics.Clear(Color.White);
            foreach (ColoredPoint cp in tvd.HighlightedNodes)
            {
                SolidBrush blackPen = new SolidBrush(cp.color);
                graphics.FillEllipse(blackPen, cp.X, cp.Y, 20, 20);
            }
            foreach (GraphicNode gNode in tvd.GraphicNodes)
            {
                String drawString = gNode.data.ToString();
                
                Font drawFont = new Font("Courier New", 14);
                SolidBrush drawBrush = new SolidBrush(Color.Black);
                
                Rectangle drawRect = new Rectangle(gNode.X, gNode.Y - SplayTree.halfY, 16 * drawString.Length, SplayTree.halfY * 2);

                // Draw rectangle to screen.
                Pen blackPen = new Pen(Color.Black, 2);
                //e.Graphics.DrawRectangle(blackPen, x, y, width, height);

                // Draw string to screen.
                
                

                //Point drawPoint = new Point(gNode.X - 20 * drawString.Length / 2, gNode.Y - SplayTree.halfY / 2);
                graphics.DrawRectangle(blackPen, drawRect);
                //graphics.DrawRectangle(Pens.Black, gNode.X - 20 * drawString.Length / 2, gNode.Y - SplayTree.halfY, 
                  //  20 * drawString.Length, 2 * SplayTree.halfY);
                graphics.DrawString(drawString, drawFont, drawBrush, drawRect);
            }
            foreach (GraphicArrow gArrow in tvd.GraphicArrows)
            {
                Pen pen = new Pen(Color.Black, 2);
                pen.StartCap = LineCap.ArrowAnchor;
                pen.EndCap = LineCap.RoundAnchor;
                graphics.DrawLine(pen, gArrow.toX, gArrow.toY, gArrow.fromX, gArrow.fromY);
            }
            statusTextBox.Text = tvd.ProgramStatus;
        }

        private void refreshOperationList()
        {
            splayTree = new SplayTree();
            for (int i = 0; i < operationList.Count; ++i)
            {
                if (i == operationList.Count - 1)
                {
                    splayTree.VisualModeEnabled = true;
                }
                if (operationList[i].Type == '+')
                {
                    splayTree.Insert(operationList[i].Data);
                    operationList[i].Result = "Inserted";
                }
                else if (operationList[i].Type == '-')
                {
                    bool res = splayTree.Delete(operationList[i].Data);
                    operationList[i].Result = res ? "Deleted" : "Not found";
                }
                else
                {
                    bool res = splayTree.Search(operationList[i].Data);
                    operationList[i].Result = res ? "Found" : "Not found";
                }
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    operationList = FileMaster.ReadFromFile(openFileDialog.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("Bad file format!", "File opening error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                return;
            }
            refreshOperationList();
            updateDataGridView();
            if (splayTree.stepsVisualDatas != null && splayTree.stepsVisualDatas.Count > 0)
                drawStep(splayTree.stepsVisualDatas[currentStepIndex]);
            this.Text = openFileDialog.FileName + " - Splay Tree Visualization";
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.ShowDialog();
            if (saveFileDialog.FileName != "")
            {
                try
                {
                    FileMaster.WriteToFile(saveFileDialog.FileName, operationList);
                }
                catch (Exception)
                {
                    MessageBox.Show("Can't write to selected file!", "File saving error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            updateDataGridView();
            if (saveFileDialog.FileName != "")
                this.Text = saveFileDialog.FileName + " - Splay Tree Visualization";
            else
                this.Text = "Splay Tree Visualization";
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            operationList = new OperationList();
            updateDataGridView();
            graphics.Clear(Color.White);
            splayTree = new SplayTree();
            splayTree.VisualModeEnabled = true;
            this.Text = "Splay Tree Visualization";
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void advancedEditButton_Click(object sender, EventArgs e)
        {
            AdvancedEditForm advEditForm = new AdvancedEditForm(operationList);
            advEditForm.ShowDialog();
            
            this.operationList = advEditForm.OperList;
            refreshOperationList();
            updateDataGridView();

            currentStepIndex = 0;
            if (splayTree.VisualModeEnabled)
            {
                if (splayTree.stepsVisualDatas != null && splayTree.stepsVisualDatas.Count > 0)
                {
                    drawStep(splayTree.stepsVisualDatas[0]);
                }
            }
        }

        private int currentStepIndex;

        private void searchButton_Click(object sender, EventArgs e)
        {
            if (operationList.Count == 5000)
            {
                MessageBox.Show("Too much operations in the file!", "Processing operation error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            splayTree.VisualModeEnabled = true;
            int data = Int32.Parse(dataTextBox.Text);
            string result = splayTree.Search(data) ? "Found" : "Not found";

            operationList.Add(new Operation(operationList.Count + 1, '?', data, result));
            updateDataGridView();
            currentStepIndex = 0;
            try
            {
                drawStep(splayTree.stepsVisualDatas[currentStepIndex]);
            }
            catch (Exception)
            {
                graphics.Clear(Color.White);
            }
        }

        private void insertButton_Click(object sender, EventArgs e)
        {
            if (operationList.Count == 5000)
            {
                MessageBox.Show("Too much operations in the file!", "Processing operation error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            splayTree.VisualModeEnabled = true;
            int data = Int32.Parse(dataTextBox.Text);
            splayTree.Insert(data);
            operationList.Add(new Operation(operationList.Count + 1, '+', data, "Inserted"));
            updateDataGridView();
            currentStepIndex = 0;
            try
            {
                drawStep(splayTree.stepsVisualDatas[currentStepIndex]);
            }
            catch (Exception)
            {
                graphics.Clear(Color.White);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (operationList.Count == 5000)
            {
                MessageBox.Show("Too much operations in the file!", "Processing operation error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            splayTree.VisualModeEnabled = true;
            int data = Int32.Parse(dataTextBox.Text);
            string result = splayTree.Delete(data) ? "Deleted" : "Not found";
            operationList.Add(new Operation(operationList.Count + 1, '-', data, result));
            updateDataGridView();
            currentStepIndex = 0;
            try
            {
                drawStep(splayTree.stepsVisualDatas[currentStepIndex]);
            }
            catch (Exception)
            {
                graphics.Clear(Color.White);
            }
        }

        private void dataTextBox_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                Int32.Parse(dataTextBox.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Input must be an integer number from -2 147 483 648 to 2 147 483 647!", "Data entry error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                dataTextBox.Focus();
            }
        }

        private void pictureBox_MouseEnter(object sender, EventArgs e)
        {
            Cursor = Cursors.SizeAll;
        }

        private void pictureBox_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private int previousMouseX;
        private int previousMouseY;

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            previousMouseX = Cursor.Position.X;
            previousMouseY = Cursor.Position.Y;
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            graphics.TranslateTransform((Cursor.Position.X - previousMouseX) / currentScale, (Cursor.Position.Y - previousMouseY) / currentScale);
            if (splayTree.VisualModeEnabled)
                drawStep(splayTree.stepsVisualDatas[currentStepIndex]);
        }

        private float currentScale;

        private void zoomOutButton_Click(object sender, EventArgs e)
        {
            graphics.ScaleTransform(1.0f / currentScale, 1.0f / currentScale);
            currentScale -= 0.1f;
            if (currentScale < 0.01f) currentScale = 0.01f;
            currentScaleTextBox.Text = (currentScale * 100).ToString();
            graphics.ScaleTransform(currentScale, currentScale);
            if (splayTree.VisualModeEnabled)
                drawStep(splayTree.stepsVisualDatas[currentStepIndex]);
        }

        private void zoomInButton_Click(object sender, EventArgs e)
        {
            graphics.ScaleTransform(1.0f / currentScale, 1.0f / currentScale);
            currentScale += 0.1f;
            if (currentScale > 3.0f) currentScale = 3.0f;
            currentScaleTextBox.Text = (currentScale * 100).ToString();
            graphics.ScaleTransform(currentScale, currentScale);
            if (splayTree.VisualModeEnabled)
                drawStep(splayTree.stepsVisualDatas[currentStepIndex]);
        }
        
        private void moveToRootButton_Click(object sender, EventArgs e)
        {
            if (splayTree != null && splayTree.VisualModeEnabled)
            {
                if (splayTree.stepsVisualDatas != null && splayTree.stepsVisualDatas.Count > 0 && splayTree.stepsVisualDatas[currentStepIndex].GraphicNodes != null && splayTree.stepsVisualDatas[currentStepIndex].GraphicNodes.Count > 0)
                {
                    graphics.TranslateTransform((-splayTree.stepsVisualDatas[currentStepIndex].GraphicNodes[0].X + graphics.VisibleClipBounds.X), (-splayTree.stepsVisualDatas[currentStepIndex].GraphicNodes[0].Y + graphics.VisibleClipBounds.Y));
                    drawStep(splayTree.stepsVisualDatas[currentStepIndex]);
                }
            }
        }

        private void stepForwardButton_Click(object sender, EventArgs e)
        {
            if (splayTree.VisualModeEnabled)
            {
                currentStepIndex = Math.Min(currentStepIndex + 1, splayTree.stepsVisualDatas.Count - 1);
                drawStep(splayTree.stepsVisualDatas[currentStepIndex]);
            }
        }

        private void stepBackButton_Click(object sender, EventArgs e)
        {
            if (splayTree.VisualModeEnabled)
            {
                currentStepIndex = Math.Max(currentStepIndex - 1, 0);
                drawStep(splayTree.stepsVisualDatas[currentStepIndex]);
            }
        }

        private void delayTextBox_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                int n = Int32.Parse(delayTextBox.Text);
                if (n <= 0 || n > 10000)
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Input must be an integer number from 0 to 10000!", "Data entry error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                delayTextBox.Focus();
            }
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            if (splayTree.VisualModeEnabled)
            {
                searchButton.Enabled = false;
                insertButton.Enabled = false;
                deleteButton.Enabled = false;
                stepBackButton.Enabled = false;
                stepForwardButton.Enabled = false;
                advancedEditButton.Enabled = false;


                timer.Interval = Int32.Parse(delayTextBox.Text);
                timer.Tick += new EventHandler(timer_Tick);
                
                timer.Enabled = true;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (splayTree.VisualModeEnabled)
            {
                drawStep(splayTree.stepsVisualDatas[currentStepIndex]);
                currentStepIndex = Math.Min(currentStepIndex + 1, splayTree.stepsVisualDatas.Count - 1);
                if (currentStepIndex == splayTree.stepsVisualDatas.Count - 1)
                {
                    timer.Enabled = false;
                    searchButton.Enabled = true;
                    insertButton.Enabled = true;
                    deleteButton.Enabled = true;
                    stepBackButton.Enabled = true;
                    stepForwardButton.Enabled = true;
                    advancedEditButton.Enabled = true;
                }
            }
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            timer.Enabled = false;
            searchButton.Enabled = true;
            insertButton.Enabled = true;
            deleteButton.Enabled = true;
            stepBackButton.Enabled = true;
            stepForwardButton.Enabled = true;
            advancedEditButton.Enabled = true;

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
    }
}
