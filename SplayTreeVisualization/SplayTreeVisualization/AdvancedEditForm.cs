﻿using System;
using System.Text;
using System.Windows.Forms;

namespace SplayTreeVisualization
{
    partial class AdvancedEditForm : Form
    {
        public OperationList OperList;
        public bool Ok;

        public AdvancedEditForm(OperationList operationList)
        {
            InitializeComponent();
            Ok = false;
            this.OperList = operationList;
            StringBuilder operations = new StringBuilder();
            operations.AppendLine(operationList.Count.ToString());
            for (int i = 0; i < operationList.Count; ++i)
            {
                operations.AppendLine(operationList[i].Type.ToString() + ' ' + operationList[i].Data.ToString());
            }
            richTextBox.Text = operations.ToString();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            string[] lines = richTextBox.Text.Split('\n');
            OperationList readenOperations = new OperationList();
            try
            {
                int size = Int32.Parse(lines[0]);
                if (size < 0 || size > 5000)
                {
                    throw new Exception();
                }
                for (int i = 1; i < size + 1; ++i)
                {
                    char type = char.Parse(lines[i].Split()[0]);
                    int data = Int32.Parse(lines[i].Split()[1]);
                    if (type != '+' && type != '-' && type != '?')
                    {
                        throw new Exception();
                    }

                    readenOperations.Add(new Operation(readenOperations.Count + 1, type, data, null));
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Bad formatted text!", "Edit error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            OperList = readenOperations;
            Ok = true;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
