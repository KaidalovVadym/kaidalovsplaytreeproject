﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplayTreeVisualization
{
    struct GraphicArrow
    {
        public int fromX;
        public int fromY;
        public int toX;
        public int toY;

        public GraphicArrow(int fromX, int fromY, int toX, int toY)
        {
            this.fromX = fromX;
            this.fromY = fromY;
            this.toX = toX;
            this.toY = toY;
        }
    }
}
