﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SplayTreeVisualization
{
    struct ColoredPoint
    {
        public Color color;
        public int X;
        public int Y;

        public ColoredPoint(int X, int Y, Color color)
        {
            this.X = X;
            this.Y = Y;
            this.color = color;
        }
    }
}
