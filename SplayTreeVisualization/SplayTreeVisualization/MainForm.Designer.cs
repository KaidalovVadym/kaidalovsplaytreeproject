﻿namespace SplayTreeVisualization
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scaleGroupBox = new System.Windows.Forms.GroupBox();
            this.moveToRootButton = new System.Windows.Forms.Button();
            this.currentScaleTextBox = new System.Windows.Forms.TextBox();
            this.currentScaleLabel = new System.Windows.Forms.Label();
            this.zoomInButton = new System.Windows.Forms.Button();
            this.zoomOutButton = new System.Windows.Forms.Button();
            this.operationsGroupBox = new System.Windows.Forms.GroupBox();
            this.dataTextBox = new System.Windows.Forms.TextBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.insertButton = new System.Windows.Forms.Button();
            this.searchButton = new System.Windows.Forms.Button();
            this.performOperationLabel = new System.Windows.Forms.Label();
            this.advancedEditButton = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.numberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resultDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.operationListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.performedOperationsLabel = new System.Windows.Forms.Label();
            this.animationGroupBox = new System.Windows.Forms.GroupBox();
            this.stepForwardButton = new System.Windows.Forms.Button();
            this.pauseButton = new System.Windows.Forms.Button();
            this.playButton = new System.Windows.Forms.Button();
            this.stepBackButton = new System.Windows.Forms.Button();
            this.delayLabel = new System.Windows.Forms.Label();
            this.delayTextBox = new System.Windows.Forms.TextBox();
            this.statusGroupBox = new System.Windows.Forms.GroupBox();
            this.statusTextBox = new System.Windows.Forms.TextBox();
            this.pictureGroupBox = new System.Windows.Forms.GroupBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip.SuspendLayout();
            this.scaleGroupBox.SuspendLayout();
            this.operationsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operationListBindingSource)).BeginInit();
            this.animationGroupBox.SuspendLayout();
            this.statusGroupBox.SuspendLayout();
            this.pictureGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1283, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // scaleGroupBox
            // 
            this.scaleGroupBox.Controls.Add(this.moveToRootButton);
            this.scaleGroupBox.Controls.Add(this.currentScaleTextBox);
            this.scaleGroupBox.Controls.Add(this.currentScaleLabel);
            this.scaleGroupBox.Controls.Add(this.zoomInButton);
            this.scaleGroupBox.Controls.Add(this.zoomOutButton);
            this.scaleGroupBox.Location = new System.Drawing.Point(902, 27);
            this.scaleGroupBox.Name = "scaleGroupBox";
            this.scaleGroupBox.Size = new System.Drawing.Size(369, 68);
            this.scaleGroupBox.TabIndex = 2;
            this.scaleGroupBox.TabStop = false;
            this.scaleGroupBox.Text = "Scale";
            // 
            // moveToRootButton
            // 
            this.moveToRootButton.Image = global::SplayTreeVisualization.Properties.Resources.Root;
            this.moveToRootButton.Location = new System.Drawing.Point(241, 19);
            this.moveToRootButton.Name = "moveToRootButton";
            this.moveToRootButton.Size = new System.Drawing.Size(122, 40);
            this.moveToRootButton.TabIndex = 4;
            this.moveToRootButton.Text = "Move to Tree";
            this.moveToRootButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.moveToRootButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.moveToRootButton.UseVisualStyleBackColor = true;
            this.moveToRootButton.Click += new System.EventHandler(this.moveToRootButton_Click);
            // 
            // currentScaleTextBox
            // 
            this.currentScaleTextBox.Location = new System.Drawing.Point(147, 30);
            this.currentScaleTextBox.Name = "currentScaleTextBox";
            this.currentScaleTextBox.ReadOnly = true;
            this.currentScaleTextBox.Size = new System.Drawing.Size(42, 20);
            this.currentScaleTextBox.TabIndex = 3;
            this.currentScaleTextBox.Text = "100.0";
            this.currentScaleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // currentScaleLabel
            // 
            this.currentScaleLabel.AutoSize = true;
            this.currentScaleLabel.Location = new System.Drawing.Point(52, 33);
            this.currentScaleLabel.Name = "currentScaleLabel";
            this.currentScaleLabel.Size = new System.Drawing.Size(89, 13);
            this.currentScaleLabel.TabIndex = 2;
            this.currentScaleLabel.Text = "Current scale (%):";
            // 
            // zoomInButton
            // 
            this.zoomInButton.Image = global::SplayTreeVisualization.Properties.Resources.ZoomIn;
            this.zoomInButton.Location = new System.Drawing.Point(195, 19);
            this.zoomInButton.Name = "zoomInButton";
            this.zoomInButton.Size = new System.Drawing.Size(40, 40);
            this.zoomInButton.TabIndex = 1;
            this.zoomInButton.UseVisualStyleBackColor = true;
            this.zoomInButton.Click += new System.EventHandler(this.zoomInButton_Click);
            // 
            // zoomOutButton
            // 
            this.zoomOutButton.Image = global::SplayTreeVisualization.Properties.Resources.ZoomOut;
            this.zoomOutButton.Location = new System.Drawing.Point(6, 19);
            this.zoomOutButton.Name = "zoomOutButton";
            this.zoomOutButton.Size = new System.Drawing.Size(40, 40);
            this.zoomOutButton.TabIndex = 0;
            this.zoomOutButton.UseVisualStyleBackColor = true;
            this.zoomOutButton.Click += new System.EventHandler(this.zoomOutButton_Click);
            // 
            // operationsGroupBox
            // 
            this.operationsGroupBox.Controls.Add(this.dataTextBox);
            this.operationsGroupBox.Controls.Add(this.deleteButton);
            this.operationsGroupBox.Controls.Add(this.insertButton);
            this.operationsGroupBox.Controls.Add(this.searchButton);
            this.operationsGroupBox.Controls.Add(this.performOperationLabel);
            this.operationsGroupBox.Controls.Add(this.advancedEditButton);
            this.operationsGroupBox.Controls.Add(this.dataGridView);
            this.operationsGroupBox.Controls.Add(this.performedOperationsLabel);
            this.operationsGroupBox.Location = new System.Drawing.Point(902, 101);
            this.operationsGroupBox.Name = "operationsGroupBox";
            this.operationsGroupBox.Size = new System.Drawing.Size(369, 483);
            this.operationsGroupBox.TabIndex = 3;
            this.operationsGroupBox.TabStop = false;
            this.operationsGroupBox.Text = "Operation list";
            // 
            // dataTextBox
            // 
            this.dataTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dataTextBox.Location = new System.Drawing.Point(144, 437);
            this.dataTextBox.Name = "dataTextBox";
            this.dataTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataTextBox.Size = new System.Drawing.Size(219, 40);
            this.dataTextBox.TabIndex = 7;
            this.dataTextBox.Text = "42";
            this.dataTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.dataTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.dataTextBox_Validating);
            // 
            // deleteButton
            // 
            this.deleteButton.Image = global::SplayTreeVisualization.Properties.Resources.Delete;
            this.deleteButton.Location = new System.Drawing.Point(98, 437);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(40, 40);
            this.deleteButton.TabIndex = 6;
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // insertButton
            // 
            this.insertButton.Image = global::SplayTreeVisualization.Properties.Resources.Insert;
            this.insertButton.Location = new System.Drawing.Point(52, 437);
            this.insertButton.Name = "insertButton";
            this.insertButton.Size = new System.Drawing.Size(40, 40);
            this.insertButton.TabIndex = 5;
            this.insertButton.UseVisualStyleBackColor = true;
            this.insertButton.Click += new System.EventHandler(this.insertButton_Click);
            // 
            // searchButton
            // 
            this.searchButton.Image = global::SplayTreeVisualization.Properties.Resources.Search;
            this.searchButton.Location = new System.Drawing.Point(6, 437);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(40, 40);
            this.searchButton.TabIndex = 4;
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // performOperationLabel
            // 
            this.performOperationLabel.AutoSize = true;
            this.performOperationLabel.Location = new System.Drawing.Point(7, 421);
            this.performOperationLabel.Name = "performOperationLabel";
            this.performOperationLabel.Size = new System.Drawing.Size(116, 13);
            this.performOperationLabel.TabIndex = 3;
            this.performOperationLabel.Text = "Perform new operation:";
            // 
            // advancedEditButton
            // 
            this.advancedEditButton.Image = global::SplayTreeVisualization.Properties.Resources.Edit;
            this.advancedEditButton.Location = new System.Drawing.Point(6, 379);
            this.advancedEditButton.Name = "advancedEditButton";
            this.advancedEditButton.Size = new System.Drawing.Size(357, 38);
            this.advancedEditButton.TabIndex = 2;
            this.advancedEditButton.Text = "Operation list advanced editing";
            this.advancedEditButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.advancedEditButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.advancedEditButton.UseVisualStyleBackColor = true;
            this.advancedEditButton.Click += new System.EventHandler(this.advancedEditButton_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.AutoGenerateColumns = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numberDataGridViewTextBoxColumn,
            this.typeDataGridViewTextBoxColumn,
            this.dataDataGridViewTextBoxColumn,
            this.resultDataGridViewTextBoxColumn});
            this.dataGridView.DataSource = this.operationListBindingSource;
            this.dataGridView.Location = new System.Drawing.Point(7, 37);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(356, 336);
            this.dataGridView.TabIndex = 1;
            // 
            // numberDataGridViewTextBoxColumn
            // 
            this.numberDataGridViewTextBoxColumn.DataPropertyName = "Number";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.numberDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.numberDataGridViewTextBoxColumn.HeaderText = "Number";
            this.numberDataGridViewTextBoxColumn.Name = "numberDataGridViewTextBoxColumn";
            this.numberDataGridViewTextBoxColumn.ReadOnly = true;
            this.numberDataGridViewTextBoxColumn.Width = 80;
            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.typeDataGridViewTextBoxColumn.DataPropertyName = "Type";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.typeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.typeDataGridViewTextBoxColumn.HeaderText = "Type";
            this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
            this.typeDataGridViewTextBoxColumn.ReadOnly = true;
            this.typeDataGridViewTextBoxColumn.Width = 50;
            // 
            // dataDataGridViewTextBoxColumn
            // 
            this.dataDataGridViewTextBoxColumn.DataPropertyName = "Data";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataDataGridViewTextBoxColumn.HeaderText = "Data";
            this.dataDataGridViewTextBoxColumn.Name = "dataDataGridViewTextBoxColumn";
            this.dataDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // resultDataGridViewTextBoxColumn
            // 
            this.resultDataGridViewTextBoxColumn.DataPropertyName = "Result";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.resultDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.resultDataGridViewTextBoxColumn.HeaderText = "Result";
            this.resultDataGridViewTextBoxColumn.Name = "resultDataGridViewTextBoxColumn";
            this.resultDataGridViewTextBoxColumn.ReadOnly = true;
            this.resultDataGridViewTextBoxColumn.Width = 90;
            // 
            // operationListBindingSource
            // 
            this.operationListBindingSource.DataSource = typeof(SplayTreeVisualization.OperationList);
            // 
            // performedOperationsLabel
            // 
            this.performedOperationsLabel.AutoSize = true;
            this.performedOperationsLabel.Location = new System.Drawing.Point(7, 20);
            this.performedOperationsLabel.Name = "performedOperationsLabel";
            this.performedOperationsLabel.Size = new System.Drawing.Size(119, 13);
            this.performedOperationsLabel.TabIndex = 0;
            this.performedOperationsLabel.Text = "Performed 0 operations:";
            // 
            // animationGroupBox
            // 
            this.animationGroupBox.Controls.Add(this.stepForwardButton);
            this.animationGroupBox.Controls.Add(this.pauseButton);
            this.animationGroupBox.Controls.Add(this.playButton);
            this.animationGroupBox.Controls.Add(this.stepBackButton);
            this.animationGroupBox.Controls.Add(this.delayLabel);
            this.animationGroupBox.Controls.Add(this.delayTextBox);
            this.animationGroupBox.Location = new System.Drawing.Point(902, 590);
            this.animationGroupBox.Name = "animationGroupBox";
            this.animationGroupBox.Size = new System.Drawing.Size(369, 67);
            this.animationGroupBox.TabIndex = 4;
            this.animationGroupBox.TabStop = false;
            this.animationGroupBox.Text = "Animation";
            // 
            // stepForwardButton
            // 
            this.stepForwardButton.Image = global::SplayTreeVisualization.Properties.Resources.StepForward;
            this.stepForwardButton.Location = new System.Drawing.Point(298, 19);
            this.stepForwardButton.Name = "stepForwardButton";
            this.stepForwardButton.Size = new System.Drawing.Size(65, 40);
            this.stepForwardButton.TabIndex = 6;
            this.stepForwardButton.UseVisualStyleBackColor = true;
            this.stepForwardButton.Click += new System.EventHandler(this.stepForwardButton_Click);
            // 
            // pauseButton
            // 
            this.pauseButton.Image = global::SplayTreeVisualization.Properties.Resources.Pause;
            this.pauseButton.Location = new System.Drawing.Point(252, 19);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(40, 40);
            this.pauseButton.TabIndex = 5;
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // playButton
            // 
            this.playButton.Image = global::SplayTreeVisualization.Properties.Resources.Play;
            this.playButton.Location = new System.Drawing.Point(206, 19);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(40, 40);
            this.playButton.TabIndex = 4;
            this.playButton.UseVisualStyleBackColor = true;
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            // 
            // stepBackButton
            // 
            this.stepBackButton.Image = global::SplayTreeVisualization.Properties.Resources.StepBack;
            this.stepBackButton.Location = new System.Drawing.Point(135, 19);
            this.stepBackButton.Name = "stepBackButton";
            this.stepBackButton.Size = new System.Drawing.Size(65, 40);
            this.stepBackButton.TabIndex = 3;
            this.stepBackButton.UseVisualStyleBackColor = true;
            this.stepBackButton.Click += new System.EventHandler(this.stepBackButton_Click);
            // 
            // delayLabel
            // 
            this.delayLabel.AutoSize = true;
            this.delayLabel.Location = new System.Drawing.Point(10, 33);
            this.delayLabel.Name = "delayLabel";
            this.delayLabel.Size = new System.Drawing.Size(59, 13);
            this.delayLabel.TabIndex = 2;
            this.delayLabel.Text = "Delay (ms):";
            // 
            // delayTextBox
            // 
            this.delayTextBox.Location = new System.Drawing.Point(75, 30);
            this.delayTextBox.Name = "delayTextBox";
            this.delayTextBox.Size = new System.Drawing.Size(54, 20);
            this.delayTextBox.TabIndex = 1;
            this.delayTextBox.Text = "2000";
            this.delayTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.delayTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.delayTextBox_Validating);
            // 
            // statusGroupBox
            // 
            this.statusGroupBox.Controls.Add(this.statusTextBox);
            this.statusGroupBox.Location = new System.Drawing.Point(12, 608);
            this.statusGroupBox.Name = "statusGroupBox";
            this.statusGroupBox.Size = new System.Drawing.Size(884, 49);
            this.statusGroupBox.TabIndex = 5;
            this.statusGroupBox.TabStop = false;
            this.statusGroupBox.Text = "Program status";
            // 
            // statusTextBox
            // 
            this.statusTextBox.Location = new System.Drawing.Point(7, 20);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.Size = new System.Drawing.Size(871, 20);
            this.statusTextBox.TabIndex = 0;
            // 
            // pictureGroupBox
            // 
            this.pictureGroupBox.Controls.Add(this.pictureBox);
            this.pictureGroupBox.Location = new System.Drawing.Point(12, 27);
            this.pictureGroupBox.Name = "pictureGroupBox";
            this.pictureGroupBox.Size = new System.Drawing.Size(884, 575);
            this.pictureGroupBox.TabIndex = 6;
            this.pictureGroupBox.TabStop = false;
            this.pictureGroupBox.Text = "Picture";
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.White;
            this.pictureBox.Location = new System.Drawing.Point(6, 19);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(872, 550);
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseDown);
            this.pictureBox.MouseEnter += new System.EventHandler(this.pictureBox_MouseEnter);
            this.pictureBox.MouseLeave += new System.EventHandler(this.pictureBox_MouseLeave);
            this.pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseUp);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1283, 669);
            this.Controls.Add(this.pictureGroupBox);
            this.Controls.Add(this.statusGroupBox);
            this.Controls.Add(this.animationGroupBox);
            this.Controls.Add(this.operationsGroupBox);
            this.Controls.Add(this.scaleGroupBox);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.Text = "Splay Tree Visualization";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.scaleGroupBox.ResumeLayout(false);
            this.scaleGroupBox.PerformLayout();
            this.operationsGroupBox.ResumeLayout(false);
            this.operationsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operationListBindingSource)).EndInit();
            this.animationGroupBox.ResumeLayout(false);
            this.animationGroupBox.PerformLayout();
            this.statusGroupBox.ResumeLayout(false);
            this.statusGroupBox.PerformLayout();
            this.pictureGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.GroupBox scaleGroupBox;
        private System.Windows.Forms.Button zoomOutButton;
        private System.Windows.Forms.GroupBox operationsGroupBox;
        private System.Windows.Forms.GroupBox animationGroupBox;
        private System.Windows.Forms.GroupBox statusGroupBox;
        private System.Windows.Forms.GroupBox pictureGroupBox;
        private System.Windows.Forms.Button zoomInButton;
        private System.Windows.Forms.Button moveToRootButton;
        private System.Windows.Forms.TextBox currentScaleTextBox;
        private System.Windows.Forms.Label currentScaleLabel;
        private System.Windows.Forms.Button advancedEditButton;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label performedOperationsLabel;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Label performOperationLabel;
        private System.Windows.Forms.Button insertButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.TextBox dataTextBox;
        private System.Windows.Forms.Button stepBackButton;
        private System.Windows.Forms.Label delayLabel;
        private System.Windows.Forms.TextBox delayTextBox;
        private System.Windows.Forms.Button playButton;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.Button stepForwardButton;
        private System.Windows.Forms.TextBox statusTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn resultDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource operationListBindingSource;
        private System.Windows.Forms.Timer timer;
    }
}

