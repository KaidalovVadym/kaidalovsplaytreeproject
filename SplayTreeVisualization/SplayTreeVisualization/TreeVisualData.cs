﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SplayTreeVisualization
{
    struct TreeVisualData
    {
        public string ProgramStatus;
        public List<GraphicNode> GraphicNodes { get; set; }
        public List<GraphicArrow> GraphicArrows { get; set; }
        public List<ColoredPoint> HighlightedNodes { get; set; }
    }
}
