﻿using System;
using System.IO;

namespace SplayTreeVisualization
{
    static class FileMaster
    {
        public static OperationList ReadFromFile(string filePath)
        {
            OperationList operationList = new OperationList();

            TextReader textReader = new StreamReader(filePath);

            string firstLine = textReader.ReadLine();
            string numberOfOperationsString = firstLine.Split()[0];

            int numberOfOperations = Int32.Parse(numberOfOperationsString);
            if (numberOfOperations < 0 || numberOfOperations > 5000)
            {
                throw new Exception();
            }
            for (int i = 0; i < numberOfOperations; ++i)
            {
                string commandLine = textReader.ReadLine();
                char type = char.Parse(commandLine.Split()[0]);
                int data = Int32.Parse(commandLine.Split()[1]);
                if (type != '+' && type != '-' && type != '?')
                {
                    throw new Exception();
                }
                operationList.Add(new Operation(operationList.Count + 1, type, data, null));
            }

            return operationList;
        }

        public static void WriteToFile(string filePath, OperationList operationList)
        {
            StreamWriter streamWriter = new StreamWriter(filePath, false);
            streamWriter.WriteLine(operationList.Count);
            for (int i = 0; i < operationList.Count; ++i)
            {
                streamWriter.WriteLine(operationList[i].Type.ToString() + ' ' + operationList[i].Data.ToString());
            }
            streamWriter.Close();
        }
    }
}
