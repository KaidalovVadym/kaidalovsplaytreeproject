﻿using System.Collections.Generic;
using System.Drawing;

namespace SplayTreeVisualization
{
    class SplayTree
    {
        const int deltaY = 100;
        const int deltaX = 100;
        public const int halfY = 12;

        private class Node
        {
            public Node leftChild;
            public Node rightChild;
            public Node parent;
            public int data;
            
            public Node (int data)
            {
                leftChild = rightChild = parent = null;
                this.data = data;
            }
        }
        
        private Node root;

        public List<TreeVisualData> stepsVisualDatas;

        private Point recurCreateChildsVisualData(Node current, ref TreeVisualData treeVisualData, ref int X, int Y, Color color, List<Node> highlightedNodes)
        {
            Point leftArrowEnd = new Point();
            if (current.leftChild != null)
            {
                leftArrowEnd = recurCreateChildsVisualData(current.leftChild, ref treeVisualData, ref X, Y + deltaY, color, highlightedNodes);
            }

            X += deltaX;
            Point currentPoint = new Point(X, Y);
            treeVisualData.GraphicNodes.Add(new GraphicNode(current.data, X, Y));
            if (highlightedNodes.Contains(current))
            {
                treeVisualData.HighlightedNodes.Add(new SplayTreeVisualization.ColoredPoint(X, Y, color));
            }

            Point rightArrowEnd = new Point();
            if (current.rightChild != null)
            {
                rightArrowEnd = recurCreateChildsVisualData(current.rightChild, ref treeVisualData, ref X, Y + deltaY, color, highlightedNodes);
            }

            if (current.leftChild != null)
                treeVisualData.GraphicArrows.Add(new GraphicArrow(currentPoint.X, currentPoint.Y + halfY, leftArrowEnd.X, leftArrowEnd.Y - halfY));
            if (current.rightChild != null)
                treeVisualData.GraphicArrows.Add(new GraphicArrow(currentPoint.X, currentPoint.Y + halfY, rightArrowEnd.X, rightArrowEnd.Y - halfY));

            return currentPoint;
        }

        private void pushStep(string programStatus, Color color, List<Node> highlightedNodes)
        {
            TreeVisualData stepVisualData = new TreeVisualData();
            stepVisualData.GraphicArrows = new List<GraphicArrow>();
            stepVisualData.GraphicNodes = new List<GraphicNode>();
            stepVisualData.HighlightedNodes = new List<ColoredPoint>();
            int x = 0;
            if (root != null)
                recurCreateChildsVisualData(root, ref stepVisualData, ref x, 0, color, highlightedNodes);
            stepVisualData.ProgramStatus = programStatus;
            stepsVisualDatas.Add(stepVisualData);
        }

        private void leftRotate(Node toDowngrade)
        {
            Node toUpgrade = toDowngrade.rightChild;

            Node theHighestAccessibleParent = toDowngrade.parent;
            toUpgrade.parent = theHighestAccessibleParent;

            if (theHighestAccessibleParent != null)
            {
                if (theHighestAccessibleParent.rightChild == toDowngrade)
                {
                    theHighestAccessibleParent.rightChild = toUpgrade;
                }
                else
                {
                    theHighestAccessibleParent.leftChild = toUpgrade;
                }
            }
            else
            {
                root = toUpgrade;
            }

            toDowngrade.rightChild = toUpgrade.leftChild;
            if (toDowngrade.rightChild != null)
            {
                toDowngrade.rightChild.parent = toDowngrade;
            }

            toUpgrade.leftChild = toDowngrade;
            toDowngrade.parent = toUpgrade;
        }

        private void rightRotate(Node toDowngrade)
        {
            Node toUpgrade = toDowngrade.leftChild;

            Node theHighestAccessibleParent = toDowngrade.parent;
            toUpgrade.parent = theHighestAccessibleParent;

            if (theHighestAccessibleParent != null)
            {
                if (theHighestAccessibleParent.rightChild == toDowngrade)
                {
                    theHighestAccessibleParent.rightChild = toUpgrade;
                }
                else
                {
                    theHighestAccessibleParent.leftChild = toUpgrade;
                }
            }
            else
            {
                root = toUpgrade;
            }

            toDowngrade.leftChild = toUpgrade.rightChild;
            if (toDowngrade.leftChild != null)
            {
                toDowngrade.leftChild.parent = toDowngrade;
            }

            toUpgrade.rightChild = toDowngrade;
            toDowngrade.parent = toUpgrade;
        }

        private void splay(Node toMakeRoot)
        {
            if (VisualModeEnabled)
            {
                List<Node> highlightedNodes = new List<Node>();
                highlightedNodes.Add(toMakeRoot);
                pushStep("Splaying node " + toMakeRoot.data + ".", Color.Orange, highlightedNodes);
            }

            while (toMakeRoot.parent != null)
            {
                Node parent = toMakeRoot.parent;
                Node grandparent = toMakeRoot.parent.parent;

                if (grandparent == null)
                {
                    if (parent.leftChild == toMakeRoot)
                    {
                        if (VisualModeEnabled)
                        {
                            List<Node> highlightedNodes = new List<Node>();
                            highlightedNodes.Add(toMakeRoot);
                            highlightedNodes.Add(parent);
                            pushStep("Doing Zig rotate.", Color.Orange, highlightedNodes);
                        }
                        // zig
                        rightRotate(parent);
                    }
                    else
                    {
                        if (VisualModeEnabled)
                        {
                            List<Node> highlightedNodes = new List<Node>();
                            highlightedNodes.Add(toMakeRoot);
                            highlightedNodes.Add(parent);
                            pushStep("Doing Zag rotate.", Color.Orange, highlightedNodes);
                        }
                        // zag
                        leftRotate(parent);                        
                    }
                }
                else if (parent.leftChild == toMakeRoot && grandparent.leftChild == parent)
                {
                    if (VisualModeEnabled)
                    {
                        List<Node> highlightedNodes = new List<Node>();
                        highlightedNodes.Add(toMakeRoot);
                        highlightedNodes.Add(parent);
                        highlightedNodes.Add(grandparent);
                        pushStep("Doing ZigZig rotate.", Color.Orange, highlightedNodes);
                    }
                    // zigzig
                    rightRotate(grandparent);
                    rightRotate(parent);
                }
                else if (parent.rightChild == toMakeRoot && grandparent.rightChild == parent)
                {
                    if (VisualModeEnabled)
                    {
                        List<Node> highlightedNodes = new List<Node>();
                        highlightedNodes.Add(toMakeRoot);
                        highlightedNodes.Add(parent);
                        highlightedNodes.Add(grandparent);
                        pushStep("Doing ZagZag rotate.", Color.Orange, highlightedNodes);
                    }
                    // zagzag
                    leftRotate(grandparent);
                    leftRotate(parent);
                }
                else if (parent.leftChild == toMakeRoot && grandparent.rightChild == parent)
                {
                    if (VisualModeEnabled)
                    {
                        List<Node> highlightedNodes = new List<Node>();
                        highlightedNodes.Add(toMakeRoot);
                        highlightedNodes.Add(parent);
                        highlightedNodes.Add(grandparent);
                        pushStep("Doing ZigZag rotate.", Color.Orange, highlightedNodes);
                    }
                    // zigzag
                    rightRotate(parent);
                    leftRotate(grandparent);
                }
                else
                {
                    if (VisualModeEnabled)
                    {
                        List<Node> highlightedNodes = new List<Node>();
                        highlightedNodes.Add(toMakeRoot);
                        highlightedNodes.Add(parent);
                        highlightedNodes.Add(grandparent);
                        pushStep("Doing ZagZig rotate.", Color.Orange, highlightedNodes);
                    }
                    // zagzig
                    leftRotate(parent);
                    rightRotate(grandparent);
                }
            }

            if (VisualModeEnabled)
            {
                List<Node> highlightedNodes = new List<Node>();
                highlightedNodes.Add(toMakeRoot);
                pushStep("Node is in the root!", Color.Green, highlightedNodes);
            }
        }

        public SplayTree()
        {
            root = null;
            VisualModeEnabled = false;
        }

        public bool Search(int data) {
            Node previous = null;
            Node current = root;

            if (VisualModeEnabled)
            {
                pushStep("Looking at the root.", Color.LightSkyBlue, new List<Node>());
            }

            while (current != null && current.data != data) {
                previous = current;

                if (VisualModeEnabled)
                {
                    List<Node> highlightedNodes = new List<Node>();
                    highlightedNodes.Add(current);
                    pushStep("Looking at node " + current.data + ".", Color.LightSkyBlue, highlightedNodes);
                }

                if (data < current.data) {
                    current = current.leftChild;
                }
                else {
                    current = current.rightChild;
                }

            }

            if (current != null) {
                if (VisualModeEnabled)
                {
                    List<Node> highlightedNodes = new List<Node>();
                    highlightedNodes.Add(current);
                    pushStep("Node found!", Color.Green, highlightedNodes);
                }
                splay(current);
                return true;
            }
            else if (previous != null) {
                splay(previous);
            }
            
            if (VisualModeEnabled)
                pushStep("Node not found!", Color.Green, new List<Node>());

            return false;
        }

        public void Insert(int data) {
            Node previous = null;
            Node current = root;

            while (current != null) {
                previous = current;

                if (VisualModeEnabled)
                {
                    List<Node> highlightedNodes = new List<Node>();
                    highlightedNodes.Add(current);
                    pushStep("Looking at node " + current.data + ".", Color.LightSkyBlue, highlightedNodes);
                }

                if (data < current.data) {
                    current = current.leftChild;
                }
                else {
                    current = current.rightChild;
                }
            }

            Node newNode = new Node(data);
            newNode.parent = previous;
            
            if (previous == null) {
                root = newNode;
            }
            else if (data < previous.data) {
                previous.leftChild = newNode;
            }
            else {
                previous.rightChild = newNode;
            }

            if (VisualModeEnabled)
            {
                List<Node> highlightedNodes = new List<Node>();
                highlightedNodes.Add(newNode);
                pushStep("Adding new node.", Color.Green, highlightedNodes);
            }

            splay(newNode);
        }

        public bool Delete(int data) {
            // a data to delete will be in root or return false
            if (!Search(data)) {
                if (VisualModeEnabled)
                {
                    pushStep("Data not found.", Color.Green, new List<Node>());
                }
                return false;
            }

            if (VisualModeEnabled)
            {
                List<Node> highlightedNodes = new List<Node>();
                highlightedNodes.Add(root);
                pushStep("Node to delete: " + root.data, Color.Red, highlightedNodes);
            }

            if (root.leftChild == null) {
                root = root.rightChild;
                if (root != null)
                {
                    root.parent = null;
                }
                if (VisualModeEnabled)
                {
                    pushStep("Node deleted.", Color.Green, new List<Node>());
                }
                return true;
            }

            // cutting right subtree
            Node rightSubtreeRoot = root.rightChild;
            root.rightChild = null;
            if (rightSubtreeRoot != null) {
                rightSubtreeRoot.parent = null;
            }
            if (VisualModeEnabled)
            {
                pushStep("Cutting right subtree of current root.", Color.LightSkyBlue, new List<Node>());
            }


            // making left subtree the root and deleting data to delete
            root = root.leftChild;
        
            root.parent = null;

            // splaying maximum in a left subtree of the node to delete
            Node maxNodeOfLeftSubtree = root;
            while (maxNodeOfLeftSubtree.rightChild != null) {
                maxNodeOfLeftSubtree = maxNodeOfLeftSubtree.rightChild;
                if (VisualModeEnabled)
                {
                    List<Node> highlightedNodes = new List<Node>();
                    highlightedNodes.Add(maxNodeOfLeftSubtree);
                    pushStep("Searching maximum in the left subtree.", Color.LightSkyBlue, highlightedNodes);
                }
            }
            splay(maxNodeOfLeftSubtree);

            // merging
            root.rightChild = rightSubtreeRoot;
            if (rightSubtreeRoot != null) {
                rightSubtreeRoot.parent = root;
            }
            if (VisualModeEnabled)
            {
                List<Node> highlightedNodes = new List<Node>();
                highlightedNodes.Add(root);
                pushStep("Merging left and right subtrees.", Color.LightSkyBlue, highlightedNodes);
            }

            if (VisualModeEnabled)
            {
                pushStep("Node deleted.", Color.Green, new List<Node>());
            }

            return true;
        }
        
        private bool visualModeEnabled;

        public bool VisualModeEnabled
        {
            get
            {
                return visualModeEnabled;
            }
            set
            {
                if (value == false)
                {
                    stepsVisualDatas = null;
                }
                else
                {
                    stepsVisualDatas = new List<TreeVisualData>();
                }
                visualModeEnabled = value;
            }
        }

    }
}
