﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplayTreeVisualization
{
    struct GraphicNode
    {
        public int data;
        public int X;
        public int Y;

        public GraphicNode(int data, int X, int Y)
        {
            this.X = X;
            this.Y = Y;
            this.data = data;
        }
    }
}
