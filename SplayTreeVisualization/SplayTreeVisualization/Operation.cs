﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplayTreeVisualization
{
    class Operation
    {
        public int Number { get; set; }
        public char Type { get; set; }
        public int Data { get; set; }
        public string Result { get; set; }

        public Operation(int number, char type, int data, string result)
        {
            Number = number;
            Type = type;
            Data = data;
            Result = result;
        }
    }
}
