#include <fstream>
#include <iostream>
#include <ios>
#include <string>
#include <ctime>

const int MAX_ELEM_NUM = 10000000;
const int MAX_OPER_NUM = 1000000000;

class SplayTree
{
private:
	struct Node
	{
		Node* leftChild;
		Node* rightChild;
		Node* parent;
		int data;

		Node(const int data)
			: leftChild(nullptr), rightChild(nullptr), parent(nullptr), data(data) {}
	};

	Node* root;

	void leftRotate(Node* toDowngrade) {
		Node* toUpgrade = toDowngrade->rightChild;

		Node* theHighestAccessibleParent = toDowngrade->parent;
		toUpgrade->parent = theHighestAccessibleParent;

		if (theHighestAccessibleParent) {
			if (theHighestAccessibleParent->rightChild == toDowngrade) {
				theHighestAccessibleParent->rightChild = toUpgrade;
			}
			else {
				theHighestAccessibleParent->leftChild = toUpgrade;
			}
		}
		else {
			root = toUpgrade;
		}

		toDowngrade->rightChild = toUpgrade->leftChild;
		if (toDowngrade->rightChild) {
			toDowngrade->rightChild->parent = toDowngrade;
		}

		toUpgrade->leftChild = toDowngrade;
		toDowngrade->parent = toUpgrade;
	}

	void rightRotate(Node* toDowngrade) {
		Node* toUpgrade = toDowngrade->leftChild;

		Node* theHighestAccessibleParent = toDowngrade->parent;
		toUpgrade->parent = theHighestAccessibleParent;

		if (theHighestAccessibleParent) {
			if (theHighestAccessibleParent->rightChild == toDowngrade) {
				theHighestAccessibleParent->rightChild = toUpgrade;
			}
			else {
				theHighestAccessibleParent->leftChild = toUpgrade;
			}
		}
		else {
			root = toUpgrade;
		}

		toDowngrade->leftChild = toUpgrade->rightChild;
		if (toDowngrade->leftChild) {
			toDowngrade->leftChild->parent = toDowngrade;
		}

		toUpgrade->rightChild = toDowngrade;
		toDowngrade->parent = toUpgrade;
	}

	void splay(Node* toMakeRoot) {
		while (toMakeRoot->parent) {
			Node* parent = toMakeRoot->parent;
			Node* grandparent = toMakeRoot->parent->parent;

			if (!grandparent)
			{
				if (parent->leftChild == toMakeRoot)
				{
					// zig
					rightRotate(parent);
				}
				else
				{
					// zag
					leftRotate(parent);
				}
			}
			else if (parent->leftChild == toMakeRoot && grandparent->leftChild == parent)
			{
				// zigzig
				rightRotate(grandparent);
				rightRotate(parent);
			}
			else if (parent->rightChild == toMakeRoot && grandparent->rightChild == parent)
			{
				// zagzag
				leftRotate(grandparent);
				leftRotate(parent);
			}
			else if (parent->leftChild == toMakeRoot && grandparent->rightChild == parent)
			{
				// zigzag
				rightRotate(parent);
				leftRotate(grandparent);
			}
			else
			{
				// zagzig
				leftRotate(parent);
				rightRotate(grandparent);
			}
		}
	}

public:
	explicit SplayTree() {
		root = nullptr;
	}

	bool Search(const int data) {
		Node* previous = nullptr;
		Node* current = root;

		while (current && current->data != data) {
			previous = current;

			if (data < current->data) {
				current = current->leftChild;
			}
			else {
				current = current->rightChild;
			}
		}

		if (current) {
			splay(current);
			return true;
		}
		else if (previous) {
			splay(previous);
		}

		return false;
	}

	void Insert(const int data) {
		Node* previous = nullptr;
		Node* current = root;

		while (current) {
			previous = current;

			if (data < current->data) {
				current = current->leftChild;
			}
			else {
				current = current->rightChild;
			}
		}

		Node* newNode = new Node(data);
		newNode->parent = previous;

		if (!previous) {
			root = newNode;
		}
		else if (data < previous->data) {
			previous->leftChild = newNode;
		}
		else {
			previous->rightChild = newNode;
		}

		splay(newNode);
	}

	bool Remove(const int data) {
		// a data to delete will be in root or return false
		if (!Search(data)) {
			return false;
		}

		if (!root->leftChild) {
			Node* nodeToDelete = root;
			root = root->rightChild;
			delete nodeToDelete;
			if (root)
				root->parent = nullptr;
			return true;
		}

		// cutting right subtree
		Node* rightSubtreeRoot = root->rightChild;
		root->rightChild = nullptr;
		if (rightSubtreeRoot) {
			rightSubtreeRoot->parent = nullptr;
		}

		// making left subtree the root and deleting data to delete
		root = root->leftChild;
		delete root->parent;
		root->parent = nullptr;

		// splaying maximum in a left subtree of the node to delete
		Node* maxNodeOfLeftSubtree = root;
		while (maxNodeOfLeftSubtree->rightChild) {
			maxNodeOfLeftSubtree = maxNodeOfLeftSubtree->rightChild;
		}
		splay(maxNodeOfLeftSubtree);

		// merging
		root->rightChild = rightSubtreeRoot;
		if (rightSubtreeRoot) {
			rightSubtreeRoot->parent = root;
		}

		return true;
	}
};

enum OperationType{SEARCH, INSERT, REMOVE, ERROR};

struct Operation 
{
	OperationType type;
	int value;

	explicit Operation(OperationType type, int value) {
		this->type = type;
		this->value = value;
	}
};

Operation readOperation(std::istream& in) {
	char typeChar;
	int value;
	in >> typeChar >> value;
	if (in.fail()) {
		return Operation(ERROR, 0);
	}
	else if (typeChar == '?') {
		return Operation(SEARCH, value);
	}
	else if (typeChar == '+') {
		return Operation(INSERT, value);
	}
	else if (typeChar == '-') {
		return Operation(REMOVE, value);
	}
	else {
		return Operation(ERROR, 0);
	}
}

void delayConsole() {
	std::cout << "Enter anything to exit.";
	std::getchar();
	std::getchar();
}

int main()
{
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(0);

	std::cout << "Welcome to Splay Tree console program!" << std::endl;
    std::cout << "WARNING! A file 'TestingCommentaries.pdf' in a root of the repository is required to read!" << std::endl;

	int treeSize = 0;
	SplayTree splayTree;

    std::cout << "Enter the input file path (spaces are not allowed): ";
    std::string inputFileName;
    std::cin >> inputFileName;
    
    std::ifstream fin(inputFileName);

    if (!fin) {
        std::cout << "Incorrect file path! Exiting program." << std::endl;
        delayConsole();
        return 0;
    }

    std::string outputFileName = inputFileName + ".a";
    
    std::ofstream fout(outputFileName);

    if (!fout) {
        std::cout << "Something wrong with output file! Exiting program.";
        std::cout << std::endl;
        delayConsole();
        return 0;
    }

    std::cout << "The result will be written to '" + outputFileName + "'.";
    std::cout << std::endl;

    int numberOfOperations = 0;
    fin >> numberOfOperations;
    
    if (fin.fail() || numberOfOperations < 0 || numberOfOperations > MAX_OPER_NUM) {
        std::cout << "Number of operations format is incorrect. Exiting program.";
        std::cout << std::endl;
        fout << 'e' << std::endl;
        delayConsole();
        return 0;
    }

    unsigned int beginTime = clock();

    for (int operNum = 0; operNum < numberOfOperations; ++operNum) {
        Operation currOper = readOperation(fin);
        if (currOper.type == SEARCH) {
            if (splayTree.Search(currOper.value)) {
                fout << 's' << '\n';
            }
            else {
                fout << 'f' << '\n';
            }
        }
        else if (currOper.type == INSERT) {
            ++treeSize;
            if (treeSize > MAX_ELEM_NUM) {
                fout << 'e' << std::endl;
                std::cout << "Maximum splay tree size 10^7 is exceeded. Exiting program.";
                std::cout << std::endl;
                delayConsole();
                return 0;
            }
            
            splayTree.Insert(currOper.value);

            fout << 's' << '\n';
        }
        else if (currOper.type == REMOVE) {
            if (splayTree.Remove(currOper.value)) {
                --treeSize;
                fout << 's' << '\n';
            }
            else {
                fout << 'f' << '\n';
            }
        }
        else {
            fout << 'e' << std::endl;
            std::cout << "Operation " << operNum + 1;
            std::cout << " is in incorrect format. Exiting program.";
            std::cout << std::endl;
            delayConsole();
            return 0;
        }
    }

    unsigned int endTime = clock();

    std::cout << "The file is successfully processed in ";
    std::cout << (endTime - beginTime);
    std::cout << " milliseconds.";
    std::cout << std::endl;

	delayConsole();
	return 0;
}
