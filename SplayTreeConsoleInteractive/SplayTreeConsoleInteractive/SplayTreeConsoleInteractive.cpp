#include <iostream>

const int MAX_ELEM_NUM = 10000000;
const int MAX_OPER_NUM = 1000000000;

class SplayTree
{
private:
    struct Node
    {
        Node* leftChild;
        Node* rightChild;
        Node* parent;
        int data;

        Node(const int data)
            : leftChild(nullptr), rightChild(nullptr), parent(nullptr), data(data) {}
    };

    Node* root;

    void leftRotate(Node* toDowngrade) {
        Node* toUpgrade = toDowngrade->rightChild;

        Node* theHighestAccessibleParent = toDowngrade->parent;
        toUpgrade->parent = theHighestAccessibleParent;

        if (theHighestAccessibleParent) {
            if (theHighestAccessibleParent->rightChild == toDowngrade) {
                theHighestAccessibleParent->rightChild = toUpgrade;
            }
            else {
                theHighestAccessibleParent->leftChild = toUpgrade;
            }
        }
        else {
            root = toUpgrade;
        }

        toDowngrade->rightChild = toUpgrade->leftChild;
        if (toDowngrade->rightChild) {
            toDowngrade->rightChild->parent = toDowngrade;
        }

        toUpgrade->leftChild = toDowngrade;
        toDowngrade->parent = toUpgrade;
    }

    void rightRotate(Node* toDowngrade) {
        Node* toUpgrade = toDowngrade->leftChild;

        Node* theHighestAccessibleParent = toDowngrade->parent;
        toUpgrade->parent = theHighestAccessibleParent;

        if (theHighestAccessibleParent) {
            if (theHighestAccessibleParent->rightChild == toDowngrade) {
                theHighestAccessibleParent->rightChild = toUpgrade;
            }
            else {
                theHighestAccessibleParent->leftChild = toUpgrade;
            }
        }
        else {
            root = toUpgrade;
        }

        toDowngrade->leftChild = toUpgrade->rightChild;
        if (toDowngrade->leftChild) {
            toDowngrade->leftChild->parent = toDowngrade;
        }

        toUpgrade->rightChild = toDowngrade;
        toDowngrade->parent = toUpgrade;
    }

    void splay(Node* toMakeRoot) {
        while (toMakeRoot->parent) {
            Node* parent = toMakeRoot->parent;
            Node* grandparent = toMakeRoot->parent->parent;

            if (!grandparent)
            {
                if (parent->leftChild == toMakeRoot)
                {
                    // zig
                    rightRotate(parent);
                }
                else
                {
                    // zag
                    leftRotate(parent);
                }
            }
            else if (parent->leftChild == toMakeRoot && grandparent->leftChild == parent)
            {
                // zigzig
                rightRotate(grandparent);
                rightRotate(parent);
            }
            else if (parent->rightChild == toMakeRoot && grandparent->rightChild == parent)
            {
                // zagzag
                leftRotate(grandparent);
                leftRotate(parent);
            }
            else if (parent->leftChild == toMakeRoot && grandparent->rightChild == parent)
            {
                // zigzag
                rightRotate(parent);
                leftRotate(grandparent);
            }
            else
            {
                // zagzig
                leftRotate(parent);
                rightRotate(grandparent);
            }
        }
    }

public:
    explicit SplayTree() {
        root = nullptr;
    }

    bool Search(const int data) {
        Node* previous = nullptr;
        Node* current = root;

        while (current && current->data != data) {
            previous = current;

            if (data < current->data) {
                current = current->leftChild;
            }
            else {
                current = current->rightChild;
            }
        }

        if (current) {
            splay(current);
            return true;
        }
        else if (previous) {
            splay(previous);
        }

        return false;
    }

    void Insert(const int data) {
        Node* previous = nullptr;
        Node* current = root;

        while (current) {
            previous = current;

            if (data < current->data) {
                current = current->leftChild;
            }
            else {
                current = current->rightChild;
            }
        }

        Node* newNode = new Node(data);
        newNode->parent = previous;

        if (!previous) {
            root = newNode;
        }
        else if (data < previous->data) {
            previous->leftChild = newNode;
        }
        else {
            previous->rightChild = newNode;
        }

        splay(newNode);
    }

    bool Remove(const int data) {
        // a data to delete will be in root or return false
        if (!Search(data)) {
            return false;
        }

        if (!root->leftChild) {
            Node* nodeToDelete = root;
            root = root->rightChild;
            delete nodeToDelete;
            if (root)
                root->parent = nullptr;
            return true;
        }

        // cutting right subtree
        Node* rightSubtreeRoot = root->rightChild;
        root->rightChild = nullptr;
        if (rightSubtreeRoot) {
            rightSubtreeRoot->parent = nullptr;
        }

        // making left subtree the root and deleting data to delete
        root = root->leftChild;
        delete root->parent;
        root->parent = nullptr;

        // splaying maximum in a left subtree of the node to delete
        Node* maxNodeOfLeftSubtree = root;
        while (maxNodeOfLeftSubtree->rightChild) {
            maxNodeOfLeftSubtree = maxNodeOfLeftSubtree->rightChild;
        }
        splay(maxNodeOfLeftSubtree);

        // merging
        root->rightChild = rightSubtreeRoot;
        if (rightSubtreeRoot) {
            rightSubtreeRoot->parent = root;
        }

        return true;
    }
};

enum OperationType { SEARCH, INSERT, REMOVE, QUIT, ERROR };

struct Operation
{
    OperationType type;
    int value;

    explicit Operation(OperationType type, int value) {
        this->type = type;
        this->value = value;
    }
};

Operation readOperation(std::istream& in) {
    char typeChar;
    in >> typeChar;
    if (!in.fail() && typeChar == 'q') {
        return Operation(QUIT, 0);
    }
    int value;
    in >> value;
    if (in.fail()) {
        return Operation(ERROR, 0);
    }
    else if (typeChar == '?') {
        return Operation(SEARCH, value);
    }
    else if (typeChar == '+') {
        return Operation(INSERT, value);
    }
    else if (typeChar == '-') {
        return Operation(REMOVE, value);
    }
    else {
        return Operation(ERROR, 0);
    }
}

void delayConsole() {
    std::cout << "Enter anything to exit.";
    std::getchar();
    std::getchar();
}

int main()
{
    std::cout << "Welcome to Splay Tree Console program!" << std::endl;
    std::cout << "----------INTERACTIVE MODE----------" << std::endl;
    std::cout << "Just start writing some commands in such formats:" << std::endl;
    std::cout << "+ int: to insert a value" << std::endl;
    std::cout << "? int: to search a value" << std::endl;
    std::cout << "- int: to delete a value" << std::endl;
    std::cout << "q: to correctly exit program" << std::endl;
    
    int numberOfOperations = 0;
    int treeSize = 0;
    SplayTree splayTree;

    while (numberOfOperations <= MAX_OPER_NUM && treeSize <= MAX_ELEM_NUM) {
        Operation operation = readOperation(std::cin);
        ++numberOfOperations;
        if (operation.type == SEARCH) {
            if (splayTree.Search(operation.value)) {
                std::cout << "Found!" << std::endl;
            }
            else {
                std::cout << "Not found!" << std::endl;
            }
        }
        else if (operation.type == INSERT) {
            splayTree.Insert(operation.value);
            ++treeSize;
            std::cout << "Inserted!" << std::endl;
        }
        else if (operation.type == REMOVE) {
            if (splayTree.Remove(operation.value)) {
                --treeSize;
                std::cout << "Removed!" << std::endl;
            }
            else {
                std::cout << "Not found!" << std::endl;
            }
        }
        else if (operation.type == QUIT) {
            std::cout << "OK, exiting the program." << std::endl;
            break;
        }
        else {
            std::cout << "Bad operation format! Exiting the program." << std::endl;
            break;
        }
    }

    if (numberOfOperations > MAX_OPER_NUM) {
        std::cout << "Maximum operations number is exceeded (10^9)." << std::endl;
    }
    if (treeSize > MAX_ELEM_NUM) {
        std::cout << "Maximum elements number is exceeded (10^7)." << std::endl;
    }

    delayConsole();

    return 0;
}

