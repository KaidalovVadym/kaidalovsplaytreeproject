#include <iostream>
#include <ios>
#include <fstream>
#include <set>
#include <string>
#include <ctime>

const int MAX_ELEM_NUM = 10000000;
const int MAX_OPER_NUM = 1000000000;

enum OperationType { SEARCH, INSERT, REMOVE, ERROR };

struct Operation
{
    OperationType type;
    int value;

    explicit Operation(OperationType type, int value) {
        this->type = type;
        this->value = value;
    }
};

Operation readOperation(std::istream& in) {
    char typeChar;
    int value;
    in >> typeChar >> value;
    if (in.fail()) {
        return Operation(ERROR, 0);
    }
    else if (typeChar == '?') {
        return Operation(SEARCH, value);
    }
    else if (typeChar == '+') {
        return Operation(INSERT, value);
    }
    else if (typeChar == '-') {
        return Operation(REMOVE, value);
    }
    else {
        return Operation(ERROR, 0);
    }
}

void delayConsole() {
    std::cout << "Enter any key to exit.";
    std::getchar();
    std::getchar();
}

int main()
{
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);

    std::cout << "Welcome to Naive Realization Checker program!" << std::endl;
    std::cout << "WARNING! A file 'TestingCommentaries.pdf' in a root of the repository is required to read!" << std::endl;

    std::cout << "Enter input file path (spaces are not allowed): ";
    std::string inputFilePath;
    std::cin >> inputFilePath;

    std::ifstream inputFileStream(inputFilePath);
    
    if (!inputFileStream) {
        std::cout << "Wrong input file path! Exiting program." << std::endl;
        delayConsole();
        return 0;
    }

    std::string outputFilePath = inputFilePath + ".a";
    std::ifstream outputFileStream(outputFilePath);

    if (!outputFileStream) {
        std::cout << "An output file " << outputFilePath << " doesn't exist!" << std::endl;
        std::cout << "Exiting program." << std::endl;
        delayConsole();
        return 0;
    }

    std::cout << "Checking answers in '" << outputFilePath << "' file." << std::endl;

    std::multiset<int> mset;

    int numberOfOperations;
    inputFileStream >> numberOfOperations;

    if (inputFileStream.fail() || numberOfOperations < 0 || numberOfOperations > MAX_OPER_NUM) {
        std::cout << "Number of operations format in the input file is incorrect." << std::endl;
        char ansChar;
        outputFileStream >> ansChar;
        if (ansChar != 'e') {
            std::cout << "Answers are wrong ('e' missed)!" << std::endl;
        }
        else {
            std::cout << "But answers are right ('e' is in the place)!" << std::endl;
        }
        std::cout << "Exiting program." << std::endl;
        delayConsole();
        return 0;
    }

    unsigned int beginTime = clock();

    for (int operNum = 0; operNum < numberOfOperations; ++operNum) {
        Operation currOper = readOperation(inputFileStream);

        char ansChar;
        outputFileStream >> ansChar;

        if (currOper.type == SEARCH) {
            if (mset.find(currOper.value) == mset.end()) {
                if (ansChar != 'f') {
                    std::cout << "Answers are wrong (incorrect answer " << operNum + 1 << ")!";
                    std::cout << std::endl;
                    delayConsole();
                    return 0;
                }
            }
            else {
                if (ansChar != 's') {
                    std::cout << "Answers are wrong (incorrect answer " << operNum + 1 << ")!";
                    std::cout << std::endl;
                    delayConsole();
                    return 0;
                }
            }
        }
        else if (currOper.type == INSERT) {
            if (mset.size() + 1 > MAX_ELEM_NUM) {
                std::cout << "Maximum elements number 10^7 is exceeded." << std::endl;
                if (ansChar != 'e') {
                    std::cout << "Answers are wrong ('e' is missed)!" << std::endl;
                    delayConsole();
                    return 0;
                }
                else {
                    std::cout << "But answers are right ('e' is in the place)!" << std::endl;
                    break;
                }
            }

            if (ansChar != 's') {
                std::cout << "Answers are wrong (incorrect answer " << operNum + 1 << ")!";
                std::cout << std::endl;
                delayConsole();
                return 0;
            }

            mset.insert(currOper.value);
        }
        else if (currOper.type == REMOVE) {
            if (mset.find(currOper.value) == mset.end()) {
                if (ansChar != 'f') {
                    std::cout << "Answers are wrong (incorrect answer " << operNum + 1 << ")!";
                    std::cout << std::endl;
                    delayConsole();
                    return 0;
                }
            }
            else {
                mset.erase(mset.find(currOper.value));
                if (ansChar != 's') {
                    std::cout << "Answers are wrong (incorrect answer " << operNum + 1 << ")!";
                    std::cout << std::endl;
                    delayConsole();
                    return 0;
                }
            }
        }
        else {
            std::cout << "Bad operation format found." << std::endl;
            if (ansChar != 'e') {
                std::cout << "Answers are wrong (must be 'e' in the end)!";
                std::cout << std::endl;
                delayConsole();
                return 0;
            }
            else {
                std::cout << "But answers are right ('e' is in the place)!" << std::endl;
                break;
            }
        }
    }

    unsigned int endTime = clock();

    std::cout << "The files checked in ";
    std::cout << (endTime - beginTime);
    std::cout << " milliseconds.";
    std::cout << std::endl;

    std::cout << "Answers are right!" << std::endl;
    delayConsole();

    return 0;
}
